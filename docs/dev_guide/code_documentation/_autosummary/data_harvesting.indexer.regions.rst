data\_harvesting.indexer.regions
================================

.. automodule:: data_harvesting.indexer.regions

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      normalize
      regionForAddress
      regionsForFeature
   
   

   
   
   

   
   
   



