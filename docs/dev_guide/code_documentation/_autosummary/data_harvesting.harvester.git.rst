data\_harvesting.harvester.git
==============================

.. automodule:: data_harvesting.harvester.git

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      check_codemeta_harvester
      harvest_fullgitlab
      harvest_hgf_gitlabs
      harvest_project
      load_gitlabs_meta
      request_gitlab
      request_gitlab_projects
      walk_gitlab
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
   
      GitHarvester
   
   

   
   
   



