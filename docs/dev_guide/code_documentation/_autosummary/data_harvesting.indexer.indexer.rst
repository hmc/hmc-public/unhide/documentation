data\_harvesting.indexer.indexer
================================

.. automodule:: data_harvesting.indexer.indexer

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      calculate
      calculatestar
      dispatch
      extract_dict
      extract_entity_dicts
      generate_index_dict
      generic_test
      generic_type_toatts
      index_dir
      process_multi
      select_identifier
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
   
      Indexer
   
   

   
   
   



