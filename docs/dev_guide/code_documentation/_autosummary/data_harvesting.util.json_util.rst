data\_harvesting.util.json\_util
================================

.. automodule:: data_harvesting.util.json_util

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      merge_json_records
      split_json_records
      write_all_data_to_csv
      write_all_jsons_to_csv
      write_csv_to_jsonfiles
   
   

   
   
   

   
   
   



