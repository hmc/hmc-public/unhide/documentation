data\_harvesting.harvester.feed
===============================

.. automodule:: data_harvesting.harvester.feed

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      convert_micro_jsonld
      harvest_feed
      parse_feed
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
   
      FeedHarvester
   
   

   
   
   



