data\_harvesting.util
=====================

.. automodule:: data_harvesting.util

   
   
   

   
   
   

   
   
   

   
   
   



.. rubric:: Modules

.. autosummary::
   :toctree:
   :recursive:

   data_harvesting.util.config
   data_harvesting.util.data_model_util
   data_harvesting.util.data_operation
   data_harvesting.util.external_schemas
   data_harvesting.util.json_ld_util
   data_harvesting.util.json_util
   data_harvesting.util.rdf_util
   data_harvesting.util.sparql_util
   data_harvesting.util.url_util

