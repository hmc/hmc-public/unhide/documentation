data\_harvesting.harvester
==========================

.. automodule:: data_harvesting.harvester

   
   
   

   
   
   

   
   
   

   
   
   



.. rubric:: Modules

.. autosummary::
   :toctree:
   :recursive:

   data_harvesting.harvester.base
   data_harvesting.harvester.datacite
   data_harvesting.harvester.feed
   data_harvesting.harvester.git
   data_harvesting.harvester.indico
   data_harvesting.harvester.oaipmh
   data_harvesting.harvester.sitemap

