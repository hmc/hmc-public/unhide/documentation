data\_harvesting.rdfpatch
=========================

.. automodule:: data_harvesting.rdfpatch

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      from_rdfgraph
      generate_patch
      to_rdfgraph
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
   
      RDFPatch
      RDFPatchMetadata
   
   

   
   
   



