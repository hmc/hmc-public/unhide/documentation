data\_harvesting.indexer.conversions
====================================

.. automodule:: data_harvesting.indexer.conversions

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      CourseInstance
      DataDownload
      GeoShape
      Place
      ProgramMembership
      PropertyValue
      endDate
      prov__wasAttributedTo
      rdf__name
      rdfs__seeAlso
      startDate
      temporalCoverage
   
   

   
   
   

   
   
   .. rubric:: Exceptions

   .. autosummary::
   
      IDCollisionError
      UnhandledDispatchException
      UnhandledFormatException
   
   



