data\_harvesting.util.data\_operation
=====================================

.. automodule:: data_harvesting.util.data_operation

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      filter_json_data
      keep_dataset
   
   

   
   
   

   
   
   



