data\_harvesting.util.data\_model\_util
=======================================

.. automodule:: data_harvesting.util.data_model_util

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      apply_aggregator
      convert_json_unhide
      sparql_query
      upload_data
      upload_data_filepath
   
   

   
   
   

   
   
   



