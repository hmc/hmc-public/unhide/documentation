data\_harvesting.cli.cron.setup
===============================

.. automodule:: data_harvesting.cli.cron.setup

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      remove
      setup_cron
      show
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
   
      CronInterval
      CronType
   
   

   
   
   



