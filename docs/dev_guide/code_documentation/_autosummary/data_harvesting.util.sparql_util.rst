data\_harvesting.util.sparql\_util
==================================

.. automodule:: data_harvesting.util.sparql_util

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      get_update_from_file
      update_from_template
   
   

   
   
   

   
   
   



