data\_harvesting.model\_core
============================

.. automodule:: data_harvesting.model_core

   
   
   .. rubric:: Module Attributes

   .. autosummary::
   
      EntryPointStr
   
   

   
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
   
      AggregatorConfig
      AggregatorStackItem
      DataciteHarvesterConfig
      DataciteHarvesterSourceItem
      FeedHarvesterConfig
      FeedHarvesterSourceItem
      GitHarvesterConfig
      GitHarvesterGitlabItem
      HarvesterSourceBaseModel
      IndexerConfig
      IndicoHarvesterConfig
      IndicoHarvesterSourceItem
      MetadataConfig
      OAIHarvesterConfig
      OAIHarvesterConfigSourceItem
      ProcessMetadata
      SitemapHarvesterConfig
      SitemapHarvesterSourceItem
      UnhideBaseModel
      UnhideConfig
      UploaderConfig
      UrlTransform
   
   

   
   
   



