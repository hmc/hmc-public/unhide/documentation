data\_harvesting.util.json\_ld\_util
====================================

.. automodule:: data_harvesting.util.json_ld_util

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      add_missing_types
      add_missing_uris
      complete_affiliations
      convert
      convert_to_csv
      gen_id
      update_key
      valdiate_from_file
      validate
      validate_jsonld_simple
   
   

   
   
   

   
   
   



